# Requiere tener git instalado
# Si se llama con exactamente el parametro "actividades", sólo se traen las actividades.

Pen="Pendrive-LPYSD2-"$(date '+%Y-%m-%d-%H%M%S')
if [[ "$1" = "actividades" ]]
then
    SoloActividades=true
else
    SoloActividades=false
fi
# Crea carpeta y descarga archivo de instalacion.
# si se indica una URL de instrucciones se descarga tambien.
descargar(){
    Carpeta=$1
    URLDescarga=$2
    URLInstrucciones=$3

    mkdir -p $Carpeta
    wget --content-disposition -P $Carpeta $URLDescarga
    [[ ! -z $URLInstrucciones ]] && wget $URLInstrucciones -O $Carpeta/InstruccionesEnIngles.html
}

clonarActividades(){
    Destino=$1
    URL=$2
    Branch=$3
    Archivos=$4

    mkdir -p $Destino
    CarpetaClon=$Destino/proyectoGit
    git clone --depth=1 $URL --branch $Branch --single-branch $CarpetaClon
    mv $CarpetaClon/$Archivos $Destino/
    rm -rf $CarpetaClon
}

# https://gist.github.com/lukechilds/a83e1d7127b78fef38c2914c4ececc3c
get_latest_release() {
  curl --silent "https://api.github.com/repos/$1/releases/latest" | # Get latest release from GitHub api
    grep '"tag_name":' |                                            # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}

eccho() {
    echo ===== $* =====
}

####################################################################
#### App Inventor ####
AppInventor=$Pen/AppInventor

if [[ $SoloActividades = false ]]
then
    eccho AppInventor: Descargando tools oficiales para USB y Emulador...
    CarpetaTools=$AppInventor/USByEmulador
    descargar $CarpetaTools/Windows/ http://appinv.us/aisetup_windows http://appinventor.mit.edu/explore/ai2/windows.html
    descargar $CarpetaTools/Linux/ http://appinv.us/aisetup_linux_deb http://appinventor.mit.edu/explore/ai2/linux.html
    descargar $CarpetaTools/Linux/ http://appinv.us/aisetup_linux_other 
    descargar $CarpetaTools/Mac/ http://appinv.us/aisetup_mac_3.0rc3 http://appinventor.mit.edu/explore/ai2/mac.html
    descargar $CarpetaTools/Mac/ http://appinv.us/aisetup_mac

    eccho AppInventor: Descargando AI2Ultimate... 
    CarpetaAI2U=$AppInventor/Offline/Windows/
    descargar $CarpetaAI2U https://sourceforge.net/projects/ai2u/files/latest/download 
fi

eccho AppInventor: Descargando actividades LPYSD2...
clonarActividades $AppInventor/Actividades https://gitlab.com/Program-AR/lpysd2.git master templates/**/*.aia

####################################################################
#### Gobstones ####
Gobstones=$Pen/Gobstones

if [[ $SoloActividades = false ]]
then
    eccho Gobstones: Descargando instaladores...
    VersionGobstones=$(get_latest_release gobstones/gobstones-web-desktop)
    wget -P $Gobstones https://github.com/gobstones/gobstones-web-desktop/releases/download/$VersionGobstones/gobstones-jr-linux-$VersionGobstones.AppImage
    wget -P $Gobstones https://github.com/gobstones/gobstones-web-desktop/releases/download/$VersionGobstones/gobstones-jr-windows-$VersionGobstones.exe
    wget -P $Gobstones https://github.com/gobstones/gobstones-web-desktop/releases/download/$VersionGobstones/gobstones-jr-windows-ia32-$VersionGobstones.zip
fi

eccho "Gobstones: Descargando actividades LPYSD2 (GBPs)..."
clonarActividades $Gobstones/Actividades/GBPs https://github.com/gobstones/curso-lpysd2.git archivosDeProyecto ArchivosDeProyectos-Generado/*.gbp

eccho "Gobstones: Descargando actividades LPYSD2 (Curso para importar)..."
DestinoCurso=$Gobstones/Actividades/cursoEntero/content
git clone --depth=1 https://github.com/gobstones/curso-lpysd2.git --branch master --single-branch $DestinoCurso
rm -rf $DestinoCurso/.git $DestinoCurso/.travis.yml
