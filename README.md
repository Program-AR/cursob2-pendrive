# cursoB2-Pendrive

El pendrive recomendado para bajarse para dar el curso B2.

Con descargarse el contenido de la carpeta `Pendrive-LPYSD2` ya se puede usar.

Para obtener una versión actualizada correr en Linux ó en Mac el script `./actualizar.sh`
(previamente habiendo permitido su ejecución con `chmod a+x actualizar.sh`)